﻿using System;

namespace Rocket_Fight.Core
{
    public class RenderLayer
    {
        public RenderLayer()
        {
            width = Game.windowWidth;
            height = Game.windowHeight;
            layer = new RenderPoint[Game.windowHeight, Game.windowWidth];
            for (int i = 0; i < layer.GetLength(0); i++)
            {
                for (int j = 0; j < layer.GetLength(1); j++)
                {
                    layer[i, j] = new RenderPoint(' ');
                }
            }
        }
        public int width;
        public int height;
        public RenderPoint[,] layer;

        public void Render()
        {
            for (int x = 1; x <= Game.windowWidth - 1; x++)
            {
                for (int y = 1; y <= Game.windowHeight - 1; y++)
                {
                    Console.ForegroundColor = layer[x, y].color;
                    Console.Write(layer[x, y].letter);
                }

                Console.Write("\n");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

    }

    public class RenderPoint {
        public char letter;
        public ConsoleColor color = ConsoleColor.White;

        public RenderPoint(char c) 
        {
            letter = c;
        }

        public RenderPoint(char c, ConsoleColor color)
        {
            letter = c;
            this.color = color;
        }
    }
}
