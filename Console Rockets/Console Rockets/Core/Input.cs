using System;
using Rocket_Fight.Core.Network;

namespace Rocket_Fight.Core
{
    public class Input
    {

        public static bool yes;
        private static void menuInput()
        {
            if (!Console.KeyAvailable && yes) 
            {
                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.DownArrow:
                    {
                        UI.currentMenu.DownSelect();
                        break;
                    }

                    case ConsoleKey.UpArrow:
                    {
                        UI.currentMenu.UpSelect();
                        break;
                    }

                    case ConsoleKey.Enter:
                    {
                        UI.currentMenu.Activate();
                        break;
                    }

                    case ConsoleKey.Escape:
                    {
                        escapeMenu();
                        break;
                    }
                }
            }
        }

        private static void escapeMenu()
        {
            switch (UI.currentMenuEnum)
            {
                case selected_menu.PLAY_MENU:
                {
                    UI.ToMainMenu();
                    break;
                }

                case selected_menu.OPTIONS_MENU:
                {
                    UI.ToMainMenu();
                    break;
                }

                case selected_menu.JOIN_MENU:
                {
                    UI.ToPlayMenu();
                    break;
                }

                case selected_menu.HOST_MENU:
                {
                    UI.ToPlayMenu();
                    break;
                }

                case selected_menu.MAIN_MENU:
                {
                    break;
                }
            }
        }

        private static void gameInput()
        {
            if (Console.KeyAvailable && yes)
            {
                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.DownArrow:
                    {
                        if(GameLogic.client.playerSelf.yPos > 1 && GameLogic.client.playerSelf.init) {
                            GameLogic.client.playerSelf.yPos -= 1;
                            GameLogic.SelfUpdate(0);
                        }
                        break;
                    }
                    case ConsoleKey.UpArrow:
                    {
                        if(GameLogic.client.playerSelf.yPos < Game.windowHeight-1 && GameLogic.client.playerSelf.init) {
                            GameLogic.client.playerSelf.yPos += 1;
                            GameLogic.SelfUpdate(0);
                        }
                        break;
                    }
                    case ConsoleKey.LeftArrow:
                    {
                        if(GameLogic.client.playerSelf.xPos > 1 && GameLogic.client.playerSelf.init) {
                            GameLogic.client.playerSelf.yPos -= 1;
                            GameLogic.SelfUpdate(0);
                        }
                        break;
                    }
                    case ConsoleKey.RightArrow:
                    {
                        if(GameLogic.client.playerSelf.yPos < Game.windowWidth-1 && GameLogic.client.playerSelf.init) {
                            GameLogic.client.playerSelf.yPos += 1;
                            GameLogic.SelfUpdate(0);
                        }
                        break;
                    }
        
                    case ConsoleKey.Spacebar:
                    {
                        GamePacketInfo pocket = new GamePacketInfo();
                        pocket.p1name = Game.playerName;
                        pocket.p2name = "enemy";
                        pocket.p1hp = 3;
                        pocket.p2hp = 1;
                        pocket.gameObjects.Add(new Rocket(20, 20));
                        GameLogic.test = pocket;
                        GameLogic.converted = MessageFactory.createInfoPacket(pocket);
                        GameLogic.client.networkSend(GameLogic.converted,1);
                        if (GameLogic.client.playerSelf.init)
                        {
                            GameLogic.SelfUpdate(GameLogic.client.playerSelf.rotation);
                        }

                        break;
                    }

                    case ConsoleKey.Escape:
                    {
                        escapeMenu();
                        break;
                    }
                }
            }
        }

        private static void localInput()
        {
            if (Console.KeyAvailable && yes)
            {
                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.LeftArrow:
                    {
                        if(GameLogic.localGame.p1.yPos > 1 && GameLogic.localGame.p1.init) {
                            GameLogic.localGame.p1.yPos -= 1;
                            GameLogic.localGame.p1.rotation = 3;
                        }
                        break;
                    }
                    case ConsoleKey.RightArrow:
                    {
                        if(GameLogic.localGame.p1.yPos < Game.windowHeight-1 && GameLogic.localGame.p1.init) {
                            GameLogic.localGame.p1.yPos += 1;
                            GameLogic.localGame.p1.rotation = 1;
                        }
                        break;
                    }
                    case ConsoleKey.UpArrow:
                    {
                        if(GameLogic.localGame.p1.xPos > 1 && GameLogic.localGame.p1.init) {
                            GameLogic.localGame.p1.xPos -= 1;
                            GameLogic.localGame.p1.rotation = 2;
                        }
                        break;
                    }
                    case ConsoleKey.DownArrow:
                    {
                        if(GameLogic.localGame.p1.xPos < Game.windowWidth-1 && GameLogic.localGame.p1.init) {
                            GameLogic.localGame.p1.xPos += 1;
                            GameLogic.localGame.p1.rotation = 4;
                        }
                        break;
                    }
                    
                    case ConsoleKey.A:
                    {
                        if(GameLogic.localGame.p2.yPos > 1 && GameLogic.localGame.p2.init) {
                            GameLogic.localGame.p2.yPos -= 1;
                            GameLogic.localGame.p2.rotation = 3;
                        }
                        break;
                    }
                    case ConsoleKey.D:
                    {
                        if(GameLogic.localGame.p2.yPos < Game.windowHeight-1 && GameLogic.localGame.p2.init) {
                            GameLogic.localGame.p2.yPos += 1;
                            GameLogic.localGame.p2.rotation = 1;
                        }
                        break;
                    }
                    case ConsoleKey.W:
                    {
                        if(GameLogic.localGame.p2.xPos > 1 && GameLogic.localGame.p2.init) {
                            GameLogic.localGame.p2.xPos -= 1;
                            GameLogic.localGame.p2.rotation = 2;
                        }
                        break;
                    }
                    case ConsoleKey.S:
                    {
                        if(GameLogic.localGame.p2.xPos < Game.windowWidth-1 && GameLogic.localGame.p2.init) {
                            GameLogic.localGame.p2.xPos += 1;
                            GameLogic.localGame.p2.rotation = 4;
                        }
                        break;
                    }
        
                    case ConsoleKey.Spacebar:
                    {
                        if (GameLogic.localGame.p2.init)
                        {
                            GameLogic.localGame.p2shoot();
                        }
                        break;
                    }
                    
                    case ConsoleKey.M:
                    {
                        if (GameLogic.localGame.p1.init)
                        {
                            GameLogic.localGame.p1shoot();
                        }
                        break;
                    }

                    case ConsoleKey.Escape:
                    {
                        escapeMenu();
                        break;
                    }
                }
            }
        }

        public static void manageInput()
        {
            if (Game.localPlay)
            {
                localInput();
            }
            else if (Game.playing)
            {
                gameInput();
            }
            else
            {
                
                menuInput();
            }
        }
    }
}