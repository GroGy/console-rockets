﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rocket_Fight.Core
{
    enum selected_menu
    {
        MAIN_MENU,
        HOST_MENU,
        OPTIONS_MENU,
        JOIN_MENU,
        PLAY_MENU,
        INGAME,
        LOADING_HOST,
        LOADING_JOIN,
        INGAME_LOCAL
    }
    
    class UI
    {

        public static Menu currentMenu = new MainMenu();

        public static selected_menu currentMenuEnum;

        public static void ToHostMenu()
        {
            currentMenuEnum = selected_menu.HOST_MENU;
            currentMenu = new HostMenu();
            Game.Render();
        }

        public static void ToOptionsMenu()
        {
            currentMenuEnum = selected_menu.OPTIONS_MENU;
            currentMenu = new OptionsMenu();
            Game.Render();
        }

        public static void ToJoinMenu()
        {
            currentMenuEnum = selected_menu.JOIN_MENU;
            currentMenu = new JoinMenu();
            Game.Render();
        }

        public static void ToMainMenu()
        {
            currentMenuEnum = selected_menu.MAIN_MENU;
            currentMenu = new MainMenu();
            Game.Render();
        }

        public static void ToPlayMenu()
        {
            currentMenuEnum = selected_menu.PLAY_MENU;
            currentMenu = new PlayMenu();
            Game.Render();
        }

        public static void ToGame()
        {
            currentMenuEnum = selected_menu.INGAME;
            currentMenu = new GameMenu();
            Game.Render();
        }

        public static void ToHostLoading()
        {
            currentMenuEnum = selected_menu.LOADING_HOST;
            currentMenu = new HostLoadingMenu();
            Game.Render();
        }
        
        public static void ToJoinLoading()
        {
            currentMenuEnum = selected_menu.LOADING_JOIN;
            currentMenu = new JoinLoadingMenu();
            Game.Render();
        }
        public static void ToLocalGame()
        {
            currentMenuEnum = selected_menu.INGAME_LOCAL;
            currentMenu = new LocalGameMenu();
            Game.Render();
        }
    }
}
