
using System.Collections.Generic;

namespace Rocket_Fight.Core.Network
{
    public class GamePacket
    {
    }

    public class GamePacketInfo : GamePacket
    {
        public List<GameObject> gameObjects = new List<GameObject>();
        public string p1name;
        public string p2name;
        public int p1hp;
        public int p2hp;
    }

    public class GamePacketUpdate : GamePacket
    {
        public string myName;
        public int myXpos;
        public int myYpos;
        public int shootDir;
    }
}