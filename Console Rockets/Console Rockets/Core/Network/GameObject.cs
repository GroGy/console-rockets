﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rocket_Fight.Core.Network
{
    public class GameObject
    {
        public int xPos;
        public int yPos;
        public int Type;
        public int ID;
        public int rotation = 0;
        public bool remove = false;
        public virtual void Render(ref RenderLayer l) 
        { }
    }

    class Rocket : GameObject
    {
        public ConsoleColor color = ConsoleColor.Yellow;
        public Rocket(int xPos, int yPos)
        {
            Type = 1;
            this.xPos = xPos;
            this.yPos = yPos;
        }
        public override void Render(ref RenderLayer l) 
        {
            char[,] rocket = Game.getRocketTexture(rotation);
            for(int x = 0; x < rocket.GetLength(0); x++)
            {
                for(int y = 0; y < rocket.GetLength(1);y++)
                {
                    l.layer[xPos + x, yPos + y] = new RenderPoint(rocket[x, y], color);
                }
            }
        }
    }
    
    class RocketShot : GameObject
    {
        private char[] textures = {'-', '|'};
        public ConsoleColor color = ConsoleColor.White;
        public RocketShot(int xPos, int yPos)
        {
            Type = 2;
            this.xPos = xPos;
            this.yPos = yPos;
        }
        public override void Render(ref RenderLayer l)
        {
            if (rotation == 0 || rotation == 2)
            {
                l.layer[xPos,yPos] = new RenderPoint(textures[0],color);
            }
            else
            {
                l.layer[xPos,yPos] = new RenderPoint(textures[1],color);
            }
        }
    }
}
