﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using ENet;

namespace Rocket_Fight.Core.Network
{
    public delegate GamePacket GamePacketMethod( string data );
    class Communicator
    {

        public string joinIp = "127.0.0.1";
        public int port = 42069;

        public void Loop()
        {
            _readNetwork();
        }

        private void _readNetwork()
        {
            if(GameLogic.hostServer != null)
            { 
                GameLogic.hostServer.networkRead();
            }

            if (GameLogic.client != null)
            {
                GameLogic.client.networkRead();
            }
        }
    }

    class MessageFactory
    {
        private static Dictionary<byte, GamePacketMethod> map = new Dictionary<byte, GamePacketMethod>();

        public static void init()
        {
            map.Add(1,serInfoPacket);
            map.Add(2,serUpdatePacket);
        }
        
        public static GamePacket getPacket(byte channel, byte[] data)
        {
            return map[channel].Invoke(Encoding.ASCII.GetString(data));
        }

        public static GamePacket serInfoPacket(string data)
        {
            // Example: NAME:3;NAME:2|1:X:Y
            // P1-name:hp;P2-name:hp | Object: ID:xPos:Ypos

            GamePacketInfo infoPacket = new GamePacketInfo();
            
            string[] mainDataParts = data.Split('|');

            string[] playerDataParts = mainDataParts[0].Split(';');
            string[] exactPlayer1DataParts = playerDataParts[0].Split(':');
            string[] exactPlayer2DataParts = playerDataParts[1].Split(':');

            infoPacket.p1name = exactPlayer1DataParts[0];
            infoPacket.p2name = exactPlayer2DataParts[0];
            infoPacket.p1hp = Convert.ToInt32(exactPlayer1DataParts[1]);
            infoPacket.p2hp = Convert.ToInt32(exactPlayer2DataParts[1]);

            if (mainDataParts.Length > 1)
            {
               
                    string[] objectDataParts = mainDataParts[1].Split(';');
                    foreach (var part in objectDataParts)
                    {
                        string[] objectDataPartsJo = part.Split(':');
                        GameObject obj = new GameObject
                        {
                            ID = Convert.ToInt32(objectDataPartsJo[0]),
                            xPos = Convert.ToInt32(objectDataPartsJo[1]),
                            yPos = Convert.ToInt32(objectDataPartsJo[2]),
                            Type = Convert.ToInt32(objectDataPartsJo[3]),
                            rotation = Convert.ToInt32(objectDataPartsJo[4])
                        };
                        infoPacket.gameObjects.Add(obj);
                    }
                    
            }

            return infoPacket;
        }
        public static GamePacket serUpdatePacket(string data)
        {
            // Example: NAME|X:Y:Shoot
            // myName | myXpos, myYpos, myShootDirection (if 0 then not shooting)

            GamePacketUpdate updatePacket = new GamePacketUpdate();
            
            string[] mainDataParts = data.Split('|');

            string[] myStatusParts = mainDataParts[1].Split(':');
            
            updatePacket.myName = mainDataParts[0];
            updatePacket.myXpos = Convert.ToInt32(myStatusParts[0]);
            updatePacket.myYpos = Convert.ToInt32(myStatusParts[1]);
            updatePacket.shootDir = Convert.ToInt32(myStatusParts[2]);

            return updatePacket;
        }
        public static string createInfoPacket(GamePacketInfo packet)
        {
            String output =
                packet.p1name +
                ":" +
                packet.p1hp +
                ";" +
                packet.p2name +
                ":" +
                packet.p2hp +
                "|";
                foreach (var gobj in packet.gameObjects)
                {
                    string app = 
                                 gobj.ID +
                                 ":" +
                                 gobj.xPos +
                                 ":" +
                                 gobj.yPos +
                                 ":" +
                                 gobj.Type +
                                 ":" +
                                 gobj.rotation +
                                 ";";
                    output = string.Format("{0}{1}", output, app);
                }

                return output;
        }
        public static string createUpdatePacket(GamePacketUpdate packet)
        {
            return
                packet.myName +
                "|" +
                packet.myXpos +
                ":" +
                packet.myYpos +
                ":" +
                packet.shootDir;
        }
    }
}