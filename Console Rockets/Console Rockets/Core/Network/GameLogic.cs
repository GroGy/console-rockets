using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ENet;

namespace Rocket_Fight.Core.Network
{

    public class Player
    {
        public int id;
        public string name;
        public int hp;
        public int rotation;

        public bool init = false;

        // public bool shotUpgraded = false;
        public int xPos;
        public int yPos;
    }

    public class Client
    {
        public Player playerSelf;
        public Player playerEnemy;
        private Host net_client;
        private Peer server;
        public List<GameObject> gameObjects = new List<GameObject>();

        public void init(int port, string hostIp)
        {
            net_client = new Host();
            net_client.Create(null, 1);
            Address add = new Address();
            add.Port = Convert.ToUInt16(port);
            add.SetHost(hostIp);
            server = net_client.Connect(add);
            playerSelf = new Player();
            playerSelf.xPos = 0;
            playerSelf.yPos = 0;
        }

        public void deinit()
        {
            net_client.Dispose();
        }

        public void process(GamePacket p)
        {
            if (p is GamePacketInfo packet)
            {
                if (!playerSelf.init)
                {
                    playerSelf.init = true;
                }

                playerEnemy.name = packet.p2name;
                playerEnemy.hp = packet.p2hp;
                playerSelf.hp = packet.p1hp;
                foreach (var gameObject in packet.gameObjects)
                {
                    if (gameObjects.Any(u => u.ID == gameObject.ID))
                    {
                        GameObject gobk = gameObjects.Where(u => u.ID == gameObject.ID).ToList()[0];
                        gobk.xPos = gameObject.xPos;
                        gobk.yPos = gameObject.yPos;
                        gobk.rotation = gameObject.rotation;
                        gobk.Type = gameObject.Type;
                    }
                    else
                    {
                        GameObject gobk = new GameObject();
                        gobk.xPos = gameObject.xPos;
                        gobk.yPos = gameObject.yPos;
                        gobk.rotation = gameObject.rotation;
                        gobk.Type = gameObject.Type;
                        gameObjects.Add(gobk);
                    }
                }
            }
        }

        public void networkRead()
        {
            bool polled = false;
            Event netEvent;
            while (!polled)
            {
                if (net_client.CheckEvents(out netEvent) <= 0)
                {
                    if (net_client.Service(5, out netEvent) < 1)
                        break;

                    polled = true;
                }

                switch (netEvent.Type)
                {
                    case EventType.None:
                        break;

                    case EventType.Connect:
                        GameLogic.connected = true;
                        break;

                    case EventType.Disconnect:
                        GameLogic.connected = false;
                        Game.playing = false;
                        UI.ToMainMenu();
                        break;

                    case EventType.Timeout:
                        GameLogic.connected = false;
                        Game.playing = false;
                        UI.ToMainMenu();
                        break;

                    case EventType.Receive:
                        byte[] buffer = new byte[1024];
                        netEvent.Packet.CopyTo(buffer);
                        process(MessageFactory.getPacket(netEvent.ChannelID, buffer));
                        netEvent.Packet.Dispose();
                        break;
                }
            }
            
            net_client.Flush();
        }

        public void networkSend(string packetData, byte channel)
        {
            Packet packet = default(Packet);
            packet.Create(Encoding.ASCII.GetBytes(packetData));
            bool sended = server.Send(channel, ref packet);
            int i = 3;
        }
    }

    public class Server
    {
        private const int MaxClients = 2;
        public int clientCount = 0;
        private Dictionary<Peer, Player> users = new Dictionary<Peer, Player>();
        private Host net_server;
        public List<GameObject> gameObjects = new List<GameObject>();

        public void init(int port)
        {
            net_server = new Host();
            Address add = new Address();
            add.Port = Convert.ToUInt16(port);

            net_server.Create(add, MaxClients);
        }
        
        public void logic()
        {
            foreach (var user in users)
            {
                Player otherPlayer = new Player();
                var otherPlayers = users.Where(u => u.Value.id != user.Value.id).ToList();
                if (otherPlayers.Count > 0)
                {
                    otherPlayer = otherPlayers[0].Value;
                }
                GamePacketInfo packet = new GamePacketInfo();
                packet.p1name = user.Value.name;
                packet.p1hp = user.Value.hp;
                if (otherPlayer == null)
                {
                    packet.p2name = "NaN";
                    packet.p2hp = 0;
                }
                else
                {
                    packet.p2name = otherPlayer.name;
                    packet.p2hp = otherPlayer.hp;
                }
                foreach (var gobj in gameObjects)
                {
                    if (gobj != null)
                    {    packet.gameObjects.Add(gobj);

                    }
                }
                networkSend(MessageFactory.createInfoPacket(packet),user.Key,1);
            }
        }

        public void process(GamePacket p, Peer sender)
        {
            if (p is GamePacketUpdate packet)
            {
                if (users[sender] != null)
                {
                    users[sender].name = packet.myName;
                    users[sender].xPos = packet.myXpos;
                    users[sender].yPos = packet.myYpos;
                }
            }
        }

        public void networkRead()
        {
            Event netEvent;

            bool polled = false;

            while (!polled)
            {
                if (net_server.CheckEvents(out netEvent) <= 0)
                {
                    if (net_server.Service(5, out netEvent) < 1)
                        break;

                    polled = true;
                }

                switch (netEvent.Type)
                {
                    case EventType.None:
                        break;

                    case EventType.Connect:
                        Player newP = new Player();
                        newP.name = "NaN";
                        newP.id = clientCount;
                        newP.hp = 3;
                        newP.xPos = Game.windowWidth/4+Game.windowWidth/2*clientCount;
                        newP.yPos = Game.windowHeight/4+Game.windowHeight/2*clientCount;
                        newP.rotation = 1+clientCount*2;
                        Rocket r = new Rocket(newP.xPos, newP.yPos);
                        r.rotation = newP.rotation;
                        gameObjects.Add(r);
                        users.Add(netEvent.Peer,newP);
                        clientCount++;
                        break;

                    case EventType.Disconnect:
                        break;
                    
                    case EventType.Timeout:
                        break;

                    case EventType.Receive:
                    {
                        byte[] buffer = new byte[1024];
                        netEvent.Packet.CopyTo(buffer);
                        process(MessageFactory.getPacket(netEvent.ChannelID, buffer),netEvent.Peer);
                        netEvent.Packet.Dispose();
                        break;
                    }
                }
            }
            net_server.Flush();
        }

        public void networkSend(string packetData, Peer p, byte channel)
        {
            Packet packet = default(Packet);
            packet.Create(Encoding.ASCII.GetBytes(packetData));
            p.Send(channel, ref packet);
        }
    }

      public class LocalGame
      {
          public Player p1;
          public Player p2;
          public int p1RocketId;
          public int p2RocketId;
          public int id = 0;
          public int p1shot = -1;
          public int p2shot = -1;

        public List<GameObject> gameObjects = new List<GameObject>();

        public void init()
        {
            p1 = new Player();
            p1.hp = 3;
            p1.init = true;
            p1.name = Game.playerName;
            p1.xPos = Game.windowWidth/4;
            p1.yPos = Game.windowHeight/4;
            p1.rotation = 1;
            Rocket r1 = new Rocket(p1.xPos, p1.yPos);
            r1.ID = id++;
            p1RocketId = r1.ID;
            r1.rotation = p1.rotation;
            gameObjects.Add(r1);
            
            p2 = new Player();
            p2.hp = 3;
            p2.init = true;
            p2.name = GameLogic.p2name;
            p2.xPos = Game.windowWidth/4+Game.windowWidth/2;
            p2.yPos = Game.windowHeight/4+Game.windowHeight/2;
            p2.rotation = 1;
            Rocket r2 = new Rocket(p2.xPos, p2.yPos);
            r2.ID = id++;
            p2RocketId = r2.ID;
            r2.rotation = p2.rotation;
            gameObjects.Add(r2);
        }

        public void logic()
        {
            for(int i  = 0;i < gameObjects.Count; i++)
            {
                if (gameObjects[i].Type == 2) //Rocket shot
                {
                    if (gameObjects[i].xPos == p1.xPos && gameObjects[i].yPos == p1.yPos)
                    {
                        p1.hp--;
                        refresh();
                        break;
                    }

                    if (gameObjects[i].xPos == p2.xPos && gameObjects[i].yPos == p2.yPos)
                    {
                        p2.hp--;
                        refresh();
                        break;
                    }

                    if (gameObjects[i].xPos >= Game.windowWidth || gameObjects[i].xPos <= 0 || gameObjects[i].yPos >= Game.windowHeight ||
                        gameObjects[i].yPos <= 0)
                    {
                        if (p1shot == gameObjects[i].ID)
                        {
                            p1shot = -1;
                        }
                        else if (p2shot == gameObjects[i].ID)
                        {
                            p2shot = -1;
                        }
                        gameObjects[i].remove = true;
                    }
                    else
                    {
                        switch (gameObjects[i].rotation)
                        {
                            case 2:
                                gameObjects[i].xPos += 1;
                                break;
                            case 1:
                                gameObjects[i].yPos += 1;
                                break;
                            case 4:
                                gameObjects[i].xPos -= 1;
                                break;
                            case 3:
                                gameObjects[i].yPos -= 1;
                                break;
                        }
                    }
                    
                }
                

                if (gameObjects[i].Type == 1)
                {
                    if (p1RocketId == gameObjects[i].ID)
                    {
                        gameObjects[i].xPos = p1.xPos;
                        gameObjects[i].rotation = p1.rotation;
                        gameObjects[i].yPos = p1.yPos;
                    } 
                    else if (p2RocketId == gameObjects[i].ID)
                    {
                        gameObjects[i].xPos = p2.xPos;
                        gameObjects[i].rotation = p2.rotation;
                        gameObjects[i].yPos = p2.yPos;
                    }
                }
            }

            for (int i = 0; i < gameObjects.Count; i++)
            {
                if (gameObjects[i].remove)
                {
                    gameObjects.RemoveAt(i);
                }
            }

            if (p1.hp == 0 || p2.hp == 0)
            {
                Game.localPlay = false;
                UI.ToMainMenu();
            }
      }

        public void refresh()
        {
            gameObjects.Clear();
            
            p1.xPos = Game.windowWidth/4;
            p1.yPos = Game.windowHeight/4;
            p1.rotation = 1;
            Rocket r1 = new Rocket(p1.xPos, p1.yPos);
            r1.ID = id++;
            p1RocketId = r1.ID;
            r1.rotation = p1.rotation;
            gameObjects.Add(r1);
            
            p2.xPos = Game.windowWidth/4+Game.windowWidth/2;
            p2.yPos = Game.windowHeight/4+Game.windowHeight/2;
            p2.rotation = 1;
            Rocket r2 = new Rocket(p2.xPos, p2.yPos);
            r2.ID = id++;
            p2RocketId = r2.ID;
            r2.rotation = p2.rotation;
            gameObjects.Add(r2);
        }

        public void p1shoot()
        {
            if (p1shot == -1)
            {
                RocketShot rocketShot = new RocketShot(p1.xPos, p1.yPos);
                rocketShot.rotation = p1.rotation;
                rocketShot.ID = id++;
                p1shot = rocketShot.ID;
                gameObjects.Add(rocketShot);
            }
        }
        
        public void p2shoot()
        {
            if(p2shot == -1){
            RocketShot rocketShot = new RocketShot(p2.xPos,p2.yPos);
            rocketShot.rotation = p2.rotation;
            rocketShot.ID = id++;
            p2shot = rocketShot.ID;
            gameObjects.Add(rocketShot);
            }
        }
      }

      
    public class GameLogic
    {
        private const int shootCD = 3000; //3 seconds

        public static Server hostServer;
        public static Client client;

        public static string p2name = "p2";
        
        public static bool connected = false;

        public static bool hosting = false;
        public static LocalGame localGame;

        public static GamePacketInfo test;
        public static string converted;

        public static int getSelfHP()
        {
            return 1;
        }

        public static void SelfUpdate(int shoot)
        {
            if (client.playerSelf.init)
            {
                GamePacketUpdate packet = new GamePacketUpdate();
                packet.myName = Game.playerName;
                packet.myXpos = client.playerSelf.xPos;
                packet.myYpos = client.playerSelf.yPos;
                packet.shootDir = shoot;
                client.networkSend(MessageFactory.createUpdatePacket(packet),2);
            }
        }

        public static void BeginJoin()
        {
            client = new Client();
            client.init(Game.Communicator.port, Game.Communicator.joinIp);
            Game.playing = true;
        }

        public static void BeginHost()
        {
            hostServer = new Server();
            hostServer.init(Game.Communicator.port);
            hosting = true;
            Game.playing = true;
        }

        public static void BeginLocal()
        {
            localGame = new LocalGame();
            localGame.init();
            Game.localPlay = true;
            UI.ToLocalGame();
        }
    }
}