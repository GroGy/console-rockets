﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rocket_Fight.Core.Network;

namespace Rocket_Fight.Core
{
    class Button
    {
        public string text;
        Action callback;

        public Button(string text, Action callback)
        {
            this.callback = callback;
            this.text = text;
        }

        public void Activate()
        {
            callback.Invoke();
        }
    }

    class Text
    {
        public string text;
        public ConsoleColor color;
        public int xPos;
        public int yPos;

        public Text(int xpos, int ypos, string text, ConsoleColor color)
        {
            xPos = xpos;
            yPos = ypos;
            this.text = text;
            this.color = color;
        }
    }

    class LoadingBar
    {
        public int width;
        private int speed;
        public int yPos;
        private ConsoleColor color;
        public RenderPoint[] renderPoints;

        public LoadingBar(int yPos, int width, int speed, ConsoleColor color)
        {
            renderPoints = new RenderPoint[width];
            this.width = width;
            this.speed = speed;
            this.yPos = yPos;
            this.color = color;
        }

        public void PreRender(DateTime currentTime)
        {
            for (int i = 0; i < width; i++)
            {
                if (i == 0)
                {
                    renderPoints[i] = new RenderPoint('[', ConsoleColor.Magenta);
                }
                else if (i == width - 1)
                {
                    renderPoints[i] = new RenderPoint(']', ConsoleColor.Magenta);
                }
                else if (i + 1 == currentTime.Second % (width - 2))
                {
                    renderPoints[i] = new RenderPoint('#', color);
                }
                else
                {
                    renderPoints[i] = new RenderPoint('-', color);
                }
            }
        }
    }

    abstract class Menu
    {
        public bool refresh = false;
        public List<Button> buttons = new List<Button>();
        public int selected = 0;
        public List<Text> texts = new List<Text>();
        public List<LoadingBar> loadingBars = new List<LoadingBar>();

        public void Render(ref RenderLayer l)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                int myY = Game.windowHeight / 2 - buttons.Count + i;

                if (selected == i)
                {
                    l.layer[myY, l.layer.GetLength(1) / 2 - buttons[i].text.Length / 2 - 2] =
                        new RenderPoint('>', ConsoleColor.Red);
                    if (buttons[i].text.Length % 2 == 0)
                    {
                        l.layer[myY, l.layer.GetLength(1) / 2 + buttons[i].text.Length / 2 + 1] =
                            new RenderPoint('<', ConsoleColor.Red);
                    }
                    else
                    {
                        l.layer[myY, l.layer.GetLength(1) / 2 + buttons[i].text.Length / 2 + 2] =
                            new RenderPoint('<', ConsoleColor.Red);
                    }
                }

                for (int j = 0; j < buttons[i].text.Length; j++)
                {
                    l.layer[myY, l.layer.GetLength(1) / 2 - buttons[i].text.Length / 2 + j] =
                        new RenderPoint(buttons[i].text[j], ConsoleColor.Yellow);
                }
            }

            for (int i = 0; i < texts.Count; i++)
            {
                for (int j = 0; j < texts[i].text.Length; j++)
                {
                    l.layer[texts[i].yPos, texts[i].xPos - texts[i].text.Length / 2 + j] =
                        new RenderPoint(texts[i].text[j], texts[i].color);
                }
            }

            for (int i = 0; i < loadingBars.Count; i++)
            {
                loadingBars[i].PreRender(DateTime.Now);
                for (int j = 0; j < loadingBars[i].width; j++)
                {
                    l.layer[loadingBars[i].yPos, Game.windowWidth / 2 - loadingBars[i].width / 2 + j] =
                        loadingBars[i].renderPoints[j];
                }
            }
        }

        public void UpSelect()
        {
            if (selected == 0)
            {
                selected = buttons.Count - 1;
            }
            else
            {
                selected--;
            }

            Game.Render();
        }

        public void DownSelect()
        {
            if (selected == buttons.Count - 1)
            {
                selected = 0;
            }
            else
            {
                selected++;
            }

            Game.Render();
        }

        public void Activate()
        {
            if (buttons.Count > 0)
            {
                buttons[selected].Activate();
            }
        }
    }

    class MainMenu : Menu
    {
        public MainMenu()
        {
            texts.Add(new Text(Game.windowWidth / 2, 4, "Console Rockets", ConsoleColor.DarkRed));
            buttons.Add(new Button("Play", UI.ToPlayMenu));
            buttons.Add(new Button("Options", UI.ToOptionsMenu));
            buttons.Add(new Button("Exit", Game.Exit));
        }
    }

    class PlayMenu : Menu
    {
        public PlayMenu()
        {
            texts.Add(new Text(Game.windowWidth / 2, 4, "Select Multiplayer Role:", ConsoleColor.White));

            buttons.Add(new Button("Join (WIP)", UI.ToJoinMenu));
            buttons.Add(new Button("Host (WIP)", UI.ToHostMenu));
            buttons.Add(new Button("Local Game",GameLogic.BeginLocal));
        }
    }

    class HostMenu : Menu
    {
        public HostMenu()
        {
            buttons.Add(new Button("Set host port", _getPortFromUser));
            buttons.Add(new Button("Start hosting!", _startHosting));
        }

        private void _getPortFromUser()
        {
            Console.Clear();
            Console.WriteLine("Enter Port of target: ");
            string input = Console.ReadLine();
            if (int.TryParse(input, out int portInput) && input != null && input.Length <= 5 && input.Length >= 2)
            {
                if (portInput < 65536 && portInput > 0)
                {
                    Game.Communicator.port = portInput;
                }
            }

            UI.ToHostMenu();
        }

        private void _startHosting()
        {
            GameLogic.BeginHost();
            UI.ToHostLoading();
        }
    }

    class JoinMenu : Menu
    {
        public JoinMenu()
        {
            texts.Add(new Text(Game.windowWidth / 2, 4,
                "Host IP: " + Game.Communicator.joinIp + ":" + Game.Communicator.port, ConsoleColor.White));
            texts.Add(new Text(Game.windowWidth / 2, 5, "Your Nickname: " + Game.playerName, ConsoleColor.White));

            buttons.Add(new Button("Set IP", _getIPFromUser));
            buttons.Add(new Button("Change Port", _getPortFromUser));
            buttons.Add(new Button("Change nickname", _getNicknameFromUser));
            buttons.Add(new Button("Join!", _joinGame));
        }

        private void _joinGame()
        {
            if (Game.Communicator.joinIp == null || Game.Communicator.port == 0)
            {
                UI.ToJoinMenu();
                //return;
            }

            GameLogic.BeginJoin();
            UI.ToJoinLoading();
        }

        private void _getIPFromUser()
        {
            Console.Clear();
            Console.WriteLine("Enter IP of target: ");
            string input = Console.ReadLine();
            if (input != null && input.Length <= 15 && input.Length >= 7 && input.Contains('.'))
            {
                Game.Communicator.joinIp = input;
            }

            Game.Render();
            UI.ToJoinMenu();
        }

        private void _getPortFromUser()
        {
            Console.Clear();
            Console.WriteLine("Enter Port of target: ");
            string input = Console.ReadLine();
            if (int.TryParse(input, out int portInput) && input != null && input.Length <= 5 && input.Length >= 2)
            {
                if (portInput < 65536 && portInput > 0)
                {
                    Game.Communicator.port = portInput;
                }
            }

            UI.ToJoinMenu();
        }

        private void _getNicknameFromUser()
        {
            Console.Clear();
            Console.WriteLine("Enter new nickname: ");
            Game.playerName = Console.ReadLine();
            UI.ToJoinMenu();
        }
    }

    class OptionsMenu : Menu
    {
        public OptionsMenu()
        {
            buttons.Add(new Button("Create test Packet", _createTestPacket));
            if (GameLogic.converted != null)
            {
                buttons.Add(new Button("Create test Packet string", _testPacketSerialization));
                texts.Add(new Text(Game.windowWidth / 2, Game.windowHeight / 3, GameLogic.converted,
                    ConsoleColor.Magenta));
            }
        }

        private void _createTestPacket()
        {
            GamePacketInfo pocket = new GamePacketInfo();
            pocket.p1name = Game.playerName;
            pocket.p2name = "enemy";
            pocket.p1hp = 3;
            pocket.p2hp = 1;
            pocket.gameObjects[0] = new Rocket(20, 20);
            GameLogic.test = pocket;
            GameLogic.converted = MessageFactory.createInfoPacket(pocket);
            GameLogic.client.networkSend(GameLogic.converted,1);
            UI.ToOptionsMenu();
        }

        private void _testPacketSerialization()
        {
            GamePacket pocket = MessageFactory.getPacket(1, Encoding.ASCII.GetBytes(GameLogic.converted));
            UI.ToOptionsMenu();
        }
    }

    class GameMenu : Menu
    {
        public GameMenu()
        {
            if (GameLogic.client.playerSelf.init)
            {
                texts.Add(new Text(Game.windowWidth / 2, Game.windowHeight - 1,
                    GameLogic.client.playerSelf.name + ": HP: " + GameLogic.getSelfHP() + " " +
                    GameLogic.client.playerEnemy.name + " : HP: " + GameLogic.client.playerEnemy.hp, ConsoleColor.Red));
            }
        }
    }

    class JoinLoadingMenu : Menu
    {
        public JoinLoadingMenu()
        {
            texts.Add(new Text(Game.windowWidth / 2, Game.windowHeight / 2, "Joining...", ConsoleColor.Yellow));
            loadingBars.Add(new LoadingBar(Game.windowHeight - 3, 30, 0, ConsoleColor.DarkGreen));
        }
    }

    class HostLoadingMenu : Menu
    {
        public HostLoadingMenu()
        {
            texts.Add(new Text(Game.windowWidth / 2, Game.windowHeight / 2, "Waiting for players to join...",
                ConsoleColor.Yellow));
            loadingBars.Add(new LoadingBar(Game.windowHeight - 3, 30, 0, ConsoleColor.DarkGreen));
        }
    }
    class LocalGameMenu : Menu
    {
        public LocalGameMenu()
        {
            if (GameLogic.localGame.p1.init && GameLogic.localGame.p2.init)
            {
                texts.Add(new Text(Game.windowWidth / 2, Game.windowHeight - 1,
                    GameLogic.localGame.p1.name + ": HP: " + GameLogic.localGame.p1.hp + " " +
                    GameLogic.localGame.p2.name + " : HP: " + GameLogic.localGame.p2.hp, ConsoleColor.Yellow));
            }
        }
    }
}