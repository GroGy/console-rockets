﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using ENet;
using Rocket_Fight.Core.Network;

namespace Rocket_Fight.Core
{
    class Game
    {
        public static string playerName;

        public const int windowWidth = 40;
        public const int windowHeight = 40;

        private static TimeSpan _loopDuration = new TimeSpan(0);
        private static TimeSpan _refreshDuration = new TimeSpan(0);
        private static TimeSpan _loopFreq = new TimeSpan(0, 0, 0, 0, 32);
        private static TimeSpan _refreshFreq = new TimeSpan(0, 0, 0, 2, 0);
        public static Communicator Communicator = new Communicator();
        private static bool _stop = false;

        public static int timeDiviner = 0;

        public static bool playing = false;
        public static bool localPlay = false;

        public static char[,] getRocketTexture(int rotation)
        {
            if (rotation == 1)
            {
                char[,] rocket = new char[3, 4];
                for (int x = 0; x < 3; x++)
                {
                    for (int y = 0; y < 4; y++)
                    {
                        rocket[x, y] = 'X';
                    }
                }

                return rocket;
            }

            if (rotation == 2)
            {
                char[,] rocket = new char[4, 3];
                for (int x = 0; x < 4; x++)
                {
                    for (int y = 0; y < 3; y++)
                    {
                        rocket[x, y] = 'Y';
                    }
                }

                return rocket;
            }

            if (rotation == 3)
            {
                char[,] rocket = new char[3, 4];
                for (int x = 0; x < 3; x++)
                {
                    for (int y = 0; y < 4; y++)
                    {
                        rocket[x, y] = 'Z';
                    }
                }

                return rocket;
            }

            if (rotation == 4)
            {
                char[,] rocket = new char[4, 3];
                for (int x = 0; x < 4; x++)
                {
                    for (int y = 0; y < 3; y++)
                    {
                        rocket[x, y] = 'M';
                    }
                }

                return rocket;
            }

            throw new Exception("Bad rotation exception!");
        }

        public static void Start() //Zapne hru 
        {
            Console.WriteLine("Enter Name: ");
            playerName = Console.ReadLine();
            _init();
        }

        public static void Exit()
        {
            Environment.Exit(0);
        }

        private static void _init()
        {
            Console.SetWindowSize(windowWidth, windowHeight);
            Console.TreatControlCAsInput = true;
            Console.Clear();
            MessageFactory.init();
            bool success = Library.Initialize();
            UI.ToMainMenu();
            _loop();
        }

        private static void _loop()
        {
            while (_stop == false)
            {
                DateTime before = DateTime.Now;
                while (_loopDuration > _loopFreq)
                {
                    _logic();
                    _loopDuration -= _loopFreq;
                    
                    if (playing && GameLogic.hosting && GameLogic.hostServer.clientCount > 0 && UI.currentMenuEnum == selected_menu.LOADING_HOST)
                    {
                        GameLogic.BeginJoin();
                        UI.ToJoinLoading();
                    }
                    
                    if (UI.currentMenuEnum == selected_menu.INGAME)
                    {
                        Render();
                    }
                    if (UI.currentMenuEnum == selected_menu.INGAME_LOCAL && timeDiviner == 20)
                    {
                        UI.ToLocalGame();
                        Render();
                        timeDiviner = 0;
                    }
                    else
                    {
                        timeDiviner++;
                    }
                }

                while (_refreshDuration > _refreshFreq)
                {
                    if (UI.currentMenuEnum == selected_menu.LOADING_HOST)
                    {
                        UI.ToHostLoading();
                    }
                    else if (UI.currentMenuEnum == selected_menu.LOADING_JOIN)
                    {
                        UI.ToJoinLoading();
                    }
                    else if (UI.currentMenuEnum == selected_menu.INGAME)
                    {
                        UI.ToGame();
                        Render();
                    }
                    _refreshDuration -= _refreshFreq;
                }
                
                if (Console.WindowWidth != windowWidth)
                {
                    Console.WindowWidth = windowWidth;
                }

                if (Console.WindowHeight != windowHeight)
                {
                    Console.WindowHeight = windowHeight;
                }

                if (GameLogic.connected && UI.currentMenuEnum != selected_menu.INGAME)
                {
                    UI.ToGame();
                }

                DateTime after = DateTime.Now;
                _refreshDuration += after - before;
                _loopDuration += after - before;
            }
        }

        private static void _logic()
        {
            if (playing)
            {
                Communicator.Loop();
            }

            if (GameLogic.hosting)
            {
                GameLogic.hostServer.logic();
            }

            if (localPlay)
            {
                GameLogic.localGame.logic();
            }

            Input.manageInput();
        }

        public static void Render()
        {
            Input.yes = false;
            Console.Clear();
            RenderLayer l = new RenderLayer();
            if (GameLogic.connected && playing)
            {
                _renderRockets(ref l);
            }

            if (localPlay)
            {
                _renderLocalRockets(ref l);
            }

            UI.currentMenu.Render(ref l);
            l.Render();
            Input.yes = true;
        }

        private static void _renderRockets(ref RenderLayer l)
        {
            foreach (GameObject obj in GameLogic.client.gameObjects)
            {
                obj.Render(ref l);
            }
        }
        
        private static void _renderLocalRockets(ref RenderLayer l)
        {
            foreach (GameObject obj in GameLogic.localGame.gameObjects)
            {
                obj.Render(ref l);
            }
        }
    }
}